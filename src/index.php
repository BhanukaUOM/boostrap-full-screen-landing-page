<?php
  if($_SERVER["HTTPS"] != "on")
  {
      header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
      exit();
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BMCSoft Developers - Website, Web application, SEO, Android, ios, Windows Mobile App, Software Solutions Developmet</title>
    <meta name="author" content="Bhanuka Rathnayaka">
    <meta name="description" content="We build powerful websites, web applications, Search Engine Optimization (SEO), Android, ios, Windows Mobile App Development, Software Solutions Development in Sri Lanka">
    <meta name="keywords" content="Web, Website, WebApp, Mobile, Ios, Android, Windows, UWP, SEO, Developer, Sri, Lanka, Sinhala, English, css, html, html5, js, javascript, jquery, bootstrap. meanstack, ionic, php, mysql">
    
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
        <script src="/js/html5shiv.js"></script>
    <![endif]-->
</head>

<body>

  <!-- Navigation Bar -->
  <div class="sticky-top">
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
      <div class="container-fluid">
        <a href="#" class="navbar-brand">
          <img src="img/BMCSoft Logo (4).png" alt="logo" style="max-width: 50%"/>
          <button class="navbar-toggler float-right" data-toggle="collapse" data-target="#navbar">
          <span class="navbar-toggler-icon"></span>
        </button>
      </a>
        <div class="navbar-collapse" id="navbar">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="#" class="nav-link active">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Projects</a></li>
            <li class="nav-item"><a href="#" class="nav-link">About Us</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>

 <!-- Image Slider -->
 <div id="slider" class="carousel slide mr-auto ml-auto" data-ride="carousel" style="max-width: 1000px;">
  <!--<ul class="carousel-indicators">
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>    
  </ul>-->
  <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="img-fluid" alt="Web Development" src="img/web.png"/>
      </div>
      <div class="carousel-item">      
        <img class="img-fluid" alt="Web Application Development" src="img/WebApp.png"/>
        <!--<div class="carousel-caption">
          <Button class="btn btn-outline-light btn-lg">Click</Button>
        </div>-->
      </div>
      <div class="carousel-item">      
          <img class="img-fluid" alt="Search Engine Optimization" src="img/SEO.png"/>
        </div>
        <div class="carousel-item">      
          <img class="img-fluid" alt="Mobile Development" src="img/Mobile.png"/>
        </div>
        <div class="carousel-item">      
          <img class="img-fluid" alt="Software Development" src="img/softwareDev.png"/>
        </div>
    </div>
    
    <a class="carousel-control-prev" href="#slider" data-slide="prev">
      <span class="carousel-control-prev-icon text-dark"></span>
    </a>
    <a class="carousel-control-next" href="#slider" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>

  <!-- What We do -->
  <div class="jumbotron" style="margin-bottom: 0;">
    <h2 class="text-center">WHAT WE DO</h2>

    <div class="row">
      <div class="col-md-6">
        <div class="card mt-2">
          <img class="card-img-top" src="img/web-card.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">WEB DESIGN</h5>
            <p class="card-text">
              <ul>
                <li>RESPONSIVE ACROSS MULTIPLE DEVICES</li>
                <li>VISUAL SIMPLICITY AND FUNCTIONALITY</li>
                <li>SMOOTH AND OPTIMIZED USER EXPERIENCE</li>
                <li>UNIQUENESS AND EXPERTISE</li>
                <li>COMPREHENSIVE WEB SOLUTIONS THAT TAKES YOU TO THE NEXT LEVEL</li>
              </ul>
            </p>
            <a href="#" class="btn btn-danger">Go to Projects</a>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="card mt-2">
          <img class="card-img-top" src="img/seo-card.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">SEO</h5>
            <p class="card-text text-justify">Our principles of search-engine optimization (SEO) will be applied to every piece of content as we create it, not just after-the-fact, in the metadata. Who is your target market? What keywords will they use to find you? That’s what you need to know at the outset. We can certainly share our experience for the success of your business....</p>

            <a href="#" class="btn btn-danger">Go to Projects</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">      
        <div class="card mt-2">
          <img class="card-img-top" src="img/softdev-card.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">SOFTWARE DEVELOPMENT</h5>
            <p class="card-text text-justify">Stay on top with custom designed and developed cloud based web applications, desktop applications, and mobile apps that run and monitor your business and measure KPI's from anywhere.</p>
   
            <h5 class="card-title">CLOUD SERVICES</h5>
            <p class="card-text text-justify">Enter the world of cloud computing! We will be your guide. From managed cloud services to hosting, we take care of it all.</p>
            <a href="#" class="btn btn-danger">Go to Projects</a>
          </div>
        </div>
      </div>

      <div class="col-md-6">        
        <div class="card mt-2">
          <img class="card-img-top" src="img/mbile-card.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">SOFTWARE DEVELOPMENT</h5>
            <p class="card-text text-justify">Android, iOS (iPhone & iPad) and Universal Windows Apps are great ways to grow your business.<br/><br/>We develop both cross platform mobile apps built with:</p>
              <ul>
                <li>Ionic</li>
                <li>Native phone apps (iPhone & iPad applications built with Objective-C or Android apps developed with Java).</li>
              </ul>
              <a href="#" class="btn btn-danger">Go to Projects</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <footer class="footer bg-dark pt-4 pb-2">
    <div class="container text-center">
      <img src="img/fb.png" alt="Facebook" style="max-width: 60px;"/>
      <img src="img/linkedin.png" alt="Facebook" style="max-width: 60px;"/>
      <img src="img/twitter.png" alt="Facebook" style="max-width: 60px;"/>
      <img src="img/gplus.png" alt="Facebook" style="max-width: 60px;"/>
      <img src="img/youtube.png" alt="Facebook" style="max-width: 60px;"/>      
    </div>
    <hr class="bg-light"/>
    <p class="text-light text-center ">BMCSoft &copy; 2018. All Rights Reserved.</p>
  </footer>

  <script src="js/popper.min.js"></script>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>